"""
    Read an ASCII file with mapping information and create a
    dictionary with the mapping information. It saves both the regular 
    map, where the key is sourceID, fiber, channel - as it comes from the
    data stream and a reverse map, where the key is the LTDB channel in the 
    format 1AM2 (tt, segment A or B depending on phi, layer, sc index)
    
    
    NOTE: The BNL LTDB mapping is checked, the FRENCH LTDB not yet
    
"""
import pickle
import string

class ltdbMapping():
    def __init__ (self,txtMapFileName = "ltdbMapping.txt",
                   pickleMapFileName="ltdbMapping.pickle"):
        #excption handling ...
        self.pickleMapFileName = pickleMapFileName
        f = open(txtMapFileName)
        self.mapList = f.readlines()
        self.mapList = map(string.split,self.mapList[1:])


        sourceIdMap = {}
        sourceIdMap['0x410511'] = {"blade":1,"port":1,"iphi":19, "phi":1.91}  #BNL LTDB
        sourceIdMap['0x410512'] = {"blade":1,"port":2,"iphi":18, "phi":1.82}  #BNL LTDB
        sourceIdMap['0x410521'] = {"blade":2,"port":1,"iphi":20, "phi":2.01}
        sourceIdMap['0x410522'] = {"blade":2,"port":2,"iphi":21, "phi":2.11}

        self.bladePortMap = {}
        self.bladePortMap[(1,1)] = {"id":0x410511, "iphi":19, "phi":1.91}
        self.bladePortMap[(1,2)] = {"id":0x410512, "iphi":18, "phi":1.82}
        self.bladePortMap[(2,1)] = {"id":0x410521, "iphi":20, "phi":2.01}
        self.bladePortMap[(2,2)] = {"id":0x410522, "iphi":21, "phi":2.11}

        self.layerMap = {0:"P", 1:"F", 2:"M", 3:"B"}
        self.phiMap   = {19:"B", 18:"A", 20:"B", 21:"A"} #CHECK 20 and 21 !!!


        self.myMap = {}
        self.myRevMap = {}
        
    def createMap(self):

        #map SourceId,fiber,adc,channel to layer,sc,tt,eta,phi
        print "-----> INFO: creating LTDB mapping"
        ignd = 0 
        for l in self.mapList :

            blade = int(l[0])
            port  = int(l[1])
            id   = self.bladePortMap[(blade,port)]["id"]
            phi  = self.bladePortMap[(blade,port)]["phi"]
            iphi = self.bladePortMap[(blade,port)]["iphi"]

            fiber   = int(l[3])
            index   = int(l[2])
            if fiber == 0 :
                channel = index
            else :
                channel = index%(fiber*8)

            #print index, fiber, channel


            layer  = int(l[5])
            sc_idx = int(l[7])
            tt     = int(l[8])
            eta    = float(l[6])

            
            #Build key based on channel name
            
            if int(l[8]) != -1 :
                #Make a name as in the table
                hName = "{0!s}{1}_{2}{3!s}".format(tt, 
                                                   self.phiMap[iphi], 
                                                   self.layerMap[layer], 
                                                   sc_idx )
            else :
                hName = "GND_" + str(ignd)
            self.myRevMap[(iphi,hName)] = {"id":id,
                                           "fiber":fiber,
                                           "channel":channel}

            ignd += 1
            
            self.myMap[(id,fiber,channel)] = {"layer":layer,
                                  "sc":sc_idx,
                                  "tt":tt,
                                  "eta":eta,
                                  "iphi":iphi,
                                  "phi":phi,
                                  "scName":hName }
            key = (id,fiber,channel)
            print "Direct Map: ", key, " ---> ",  self.myMap[key]
            
            rkey = (iphi,hName)
            print "Reverse Map:  ", rkey, " ---> ",self.myRevMap[rkey]
            
            
        print "-----> INFO: saving pickle file ",self.pickleMapFileName

        fp = open(self.pickleMapFileName,"wb")
        mm = {"dirMap":self.myMap, "revMap":self.myRevMap}
        pickle.dump(mm, fp)
        fp.close()

if __name__ == "__main__":
    x = ltdbMapping(txtMapFileName="ltdbMapping.txt", pickleMapFileName="ltdbMapping.pickle")
    x.createMap()