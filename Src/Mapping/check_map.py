"""
CReate a table to check the mapping information for LTDB based on my pickle file and the LTDB mapping document
in https://indico.cern.ch/event/374591/contributions/889416/attachments/746291/1733864/Demonstrator_mapping_V1_40__02nov15.pdf
"""

import pickle
import string

f = open("ltdbMapping.pickle")
m = pickle.load(f)
f.close()

#print a table similar to the hardware one

idList = [0x410511, 0x410512]
fiberList = range(20)
channelList = range(8)

layerMap = {0:"P", 1:"F", 2:"M", 3:"B"}
phiMap   = {19:"B",18:"A"}

#Read the hardware documentation table and create a dictionary for comparison with the mapping used

ff = open("hardware_map.txt")
hh = ff.readlines()
hh = hh[1:]
hhh = map(string.split,hh)

#Dictionary for check

hardMap = {}
for i in hhh :
    fpga      = int(i[0])
    ltdbCh    = int(i[1])
    adcChip   = int(i[2])
    ERNIPin   = i[3]
    SCell     = i[4]
    ltdbFiber = int(i[5])
    
    hardMap[ltdbCh] = [fpga, SCell] 
 
mySoftSet = set()

for id in idList :
    for fiber in fiberList :
        for ch in channelList :
            #print hex(id), fiber, ch
            x = m[(id,fiber,ch)]
            if x["tt"] != -1 :
                #Make a name as in the table
                hName = "{0!s}{1}_{2}{3!s}".format(x["tt"], phiMap[x["iphi"]], layerMap[x["layer"]], x["sc"] )
            else :
                hName = "GND"
            
            fpga = 4 - (fiber/10 + 2*(abs(x["iphi"]-19))) 
            
            ltdbCh = (fpga-1)*80 + (fiber*8 + ch)%80  
            #print "My Map:  " ,x["tt"], x["layer"], x["sc"], " (",hName,")", " --- > ", "FPGA_"+str(fpga), "LTDB Channel: ", ltdbCh
            #print "Hard Map:", hardMap[ltdbCh][1], " ----> FPGA: ", hardMap[ltdbCh][0], "LTDB Channel: ", ltdbCh
            mySoftSet.add(ltdbCh)
            if hName == hardMap[ltdbCh][1] :
                st = "OK"
            else :
                st = "DIFF"
            print "My Map: ",  hName, "  -----> ", "Hard Map: ", hardMap[ltdbCh][1], "  (", st,")"
            
#What is in, what is not 
myHardSet = set(hardMap.keys())

print " Hardware Map Size:  ", len(myHardSet)
print " Software Map Size: (from pickle file) :", len(mySoftSet)
print " SC in Hardware Map and not in pickle Map:", myHardSet - mySoftSet







