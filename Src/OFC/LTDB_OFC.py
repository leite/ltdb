"""
    Interface class for OFC writing/reading
    
    For USP cluster, before using, do 
    
    setupATLAS ; lsetup "root 6.04.16-x86_64-slc6-gcc49-opt"
    
"""

import sys
import time
import getpass

import numpy as np

import ROOT

from   root_numpy import array2tree, tree2array


class LTDB_OFC :
    """
        This describes the LTDB_OFC class for loading and writing OFC in
        a root file according to this format bellow. The OFC are calculated in
        OFC_XXX and this class provides the interface between the generator
        and the analysis code. It also can create a "dummy" OFC array for 
        general testing.
    
        
        The Root File is composed a set of branches describing the 
        conditions used to create the OFC and an array in the format 
        described bellow containing the OFCs. Several entries can be
        present, and in this case, each entry represents a different
        condition.
        
        Notes 
        -----
        
        Metadata :
        
        These are individual branches in the ROOT Tree ::
        
            date : date of OFC creation
            time : time of OFC creation
            epoch : epoch time (usefull for DB application)
            version : version of OFC 
            user : who created it
            ofcType : a string of free format containing several conditions like
                      calibration runs used, set of waveform parameters etc.
            nParameters : number of OFC paramters
            nSamples : number of samples 
            nSCells : number of SuperCells for which we have the OFCs
            
            
        OFC Array :
        
        The OFC array is a m x p array where p = 7+nParameters*nSamples
        and m = 50*nSCells (50 is the number of different offsets used, 
        generally -25 to 24 ns. Each element is described bellow ::

            [i00 i01 ... i0p] 
            .
            .
            . 
            [im0 im1 ... imp] 
            
            i0 : layer 
            i1 : iphi
            i2 : ieta
            i3 : offset_ion
            i4 : offset_calib 
            i5 : lsb
            i6..in : OFC a0..an  (n is the number of samples)
            in+1..ip : OFC b0..bn 
            
            
        Todo :
            * include the extra parameter explanation in array
             
    """
    
    #--------------------------------------------------------------------------
    def __init__ (self):
        """
           Currently holds dfinition for generating a "dummy" OFC array for
           the demonstrator LTDBs
        """
    
        self.nEta = {0:14, 1:56, 2:56, 3:14}
        self.nPhi = 4
        self.nLayer = 4
        
        self.phiMin = 18
        self.phiMax = 21
        
        self.now = time.localtime()
        
        self.ofcArray = None
    #--------------------------------------------------------------------------
    
    #--------------------------------------------------------------------------
    def buildArray (self, 
                          version=None,
                          nParameters=None,
                          nSamples=None,
                          ofcType="Unknown",
                          ofcArray = None ) :
    
        """
           Helper to create a record array from a simple OFC array,
           adding extra information.
           
           Parameters
           ----------
           
           version : str
               The version of the OFC 
           nParameters : int
               Number of OFC parameters 
           nSamples : int 
               Number of samples for which OFC are calculated
           ofcType : str
               Describes the conditions for OFC generation
           ofcArray : numpy array of dim 2
               The OFC array 
               
           Returns
           -------
           bool 
               True if successful, False otherwise.
        """
        
        
        self.date = time.strftime("%Y-%b-%d",self.now)
        self.time = time.strftime("%H:%M:%S",self.now)
        self.epoch = int ( time.mktime(self.now) )
        
        #Who is doing this    
        self.user = getpass.getuser()

    
        #Some metadata information
        if version is None :
            #avoid repeating the version (questionable .. )
            v = time.strftime("V_%Y.%b.%d-%H.%M.%S",self.now)
            self.version = v
        else :
            self.version = version

        self.ofcType = ofcType
        
        if nSamples is None :
            print "----> ERROR : Please specify the number of samples "
            return False
        else :
            self.nSamples = nSamples
            
        if nParameters is None :
            print "----> ERROR : Please specify the number of parameters "
            return False
        else :
            self.nParameters = nParameters
        
        
        if ofcArray is None :
            print "----> WARNING : No OFC array is passed. ",
            print " WILL RUN IN TEST MODE (ALL ZEROS) "
            

            delay = np.arange(-25., 25)
            nDelay = len(delay)
            
            self.nSCells = np.sum ( self.nEta.values() ) * self.nPhi
            #The 5 factor accounts for layer, iphi, ieta, lsb, ofset calib  
            #columns
            aSize = 5 * self.nSCells * nDelay * self.nSamples * self.nParameters
            ofcArray = np.zeros(aSize)
            
            #layer,iphi,ieta,delay,lsb, offset calib, a0,..,an,b0,...bn
            n = 6 + (self.nSamples *  self.nParameters)
            m = aSize/n
            print aSize,m,n
            i = 0
            lsb = 0.0
            offCalib = 0.0
            
            ofcArray.shape = (m,n)
            for layer in range (4) :
                for iphi in range (self.nPhi) :
                    for ieta in range (self.nEta[layer]) :
                        for d in range(nDelay) :
                            
                            ofcArray[i,0:6] = \
                                [layer, iphi+self.phiMin, ieta, delay[d],
                                 lsb, offCalib]
                            i += 1
                        
        
        
        
        descriptor = [ ("date","S20"), 
                       ("time","S20"),
                       ("epoch","i4"), 
                       ("version","S20"),
                       ("user","S20"),
                       ("ofcType","S20"),
                       ("nParameters","i4"),
                       ("nSamples","i4"),
                       ("nSCells","i4"),
                       ("OFC","f4", ofcArray.shape) ]
                     
        aTuple = ( self.date, 
                   self.time, 
                   self.epoch,
                   self.version, 
                   self.user, 
                   self.ofcType,
                   self.nParameters,
                   self.nSamples,
                   self.nSCells,
                   ofcArray )
                                    
        self.ofcArray = np.array( [aTuple], dtype = descriptor )
        
        return True
    
    #--------------------------------------------------------------------------

    #--------------------------------------------------------------------------    
    def saveArray(self, ofcDBDir="OFC_DB/", outFileName="ltdb_ofc"):
        
        """Write the root file with metadata and OFC array
        
            Parameters
            ----------
            
            ofcDBDir : str
                path to the directory where OFC files are stored
            outFileName : str
                name of output file (.root will be appended to it)
                
            Returns
            -------
            bool
                True if successful, False otherwise.
            
                
            TODO :
                * check directory and filename, raise exceptions
                * if we passa file with .root, then do not append it again
        """
               
                            
        if self.ofcArray is None :
            name = ofc.__class__.__name__
            "----> ERROR : Must call ", name,".buildArray first !!"
            return False
        
        else :
            v = time.strftime("V_%Y.%b.%d-%H.%M.%S",self.now)
            #For now, we save the date in the file name also
            self.outFileName = ofcDBDir + "/" + outFileName + "." + v + ".root" 
        
            print "----> INFO : OFC will be written to ", self.outFileName
        
            self.outFile = ROOT.TFile(self.outFileName,"recreate")
            self.tree = array2tree(self.ofcArray, name="LTDB_OFC") 
            self.tree.Write()
            self.outFile.Close() 
        
        return True            
    #--------------------------------------------------------------------------
    
    #--------------------------------------------------------------------------
    def readArray(self,ofcFileName=None, entry=None):
        """
            Reads into memory OFC coefs saved into a root file
            
            Parameters
            ----------
                ofcFileName : str
                    Name of the ROOT tree containing the OFC metadata and 
                    array
                
                entry : int
                    Which entry to use. Enforced in case there is more than 
                    one entry. Otherwise, it will pick the only one available
                    
            Returns
            -------
            bool
                True if successful, False otherwise.
            
            
            TODO: 
                * check for exceptions
                * add a printout with some information about what we read
                
        """
        
        f = ROOT.TFile(ofcFileName)
        t = f.Get("LTDB_OFC")
        
        
        nEntries = self.ofcArray_r.GetEntries()
        
        if (nEntries > 1) and (entry is None) :
            print "----> ERROR : Tree has more than one condition saved !!!"
            print "----> ERROR : You must specify which one to use "
            return False
        elif nEntries == 1 :
            entry = 1
            
        #get the array with the information
        print "----> INFO : Reading entry ", entry
        self.ofcArray_r = tree2array(t,start=entry,stop=entry+1)
        f.Close()
         
        self.ofc_r = self.ofcArray_r["OFC"][0]
        
        return True
        
    #--------------------------------------------------------------------------
    
    #--------------------------------------------------------------------------
    def fixArray(self):
        """
            This is here to handle first implementation issues and will be 
            removed soon ...
        """
        
        print "----> WARNING: Fixing the OFC array"
        #non zeros index
        #iphi != 0
        nzIdx = np.where(self.ofc_r[:,1] != 0 )
        
        self.ofc_r = self.ofc_r[nzIdx]
        
        #now add the time offset information
        toffset = np.arange(-25,25)
        for i in range(self.ofc_r.shape[0]) :
            self.ofc_r[i,5] = toffset[i%50]
            
            
        
#------------------------------------------------------------------------------
# END OF CLASS DEFINITION
#------------------------------------------------------------------------------
if __name__ == "__main__":
    

    ofc  = LTDB_OFC()
    #Here we don't have an OFC calculated array, so the code will output 
    #an array with dumb coef. 
    #What we would like to have is 
    #ofc.buildArray(nParameters=2, nSamples=4, ofcArray=myOFCArray)
    #where myOFCArray is the calculated OFC array ...
    
    #ofc.buildArray(nParameters=2, nSamples=4)
    #ofc.saveArray()
     
    #Read OFC 
    ofc.readArray(ofcFileName="OFC_DB/ltdb_ofc.V_2017.Jan.25-14.07.26.root")   
    ofc.fixArray()   
        
        
        
        
        
        
        
        
        
            
        
        
        