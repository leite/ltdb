"""
    messages.py defines a generic class to print standard formatted 
    messages into output
    
"""
import sys

class message(object):
    """
        This classes format some messages types providing a common format
        across several parts of the code. This is useless standalone, and
        it is designed to be inherited by other classes. Message will be 
        collored according to the type using ANSY scapes (so no need for 
        extra python package).
    """
    
    #---------------------------------------------------------------------------
    def __init__(self, cName=None, truncate=None):
        """
            Prepare the strings for messages. Also defines scape sequences for
            ANSI terminal colors
            
            Parameters
            ----------
            
            cName : string
                The class name (or the class itself then extract the name ?)
                
            truncate :
                How many chars to print . If None, will not truncate whatever
                is passed
                
        """
        #How to get the name of the super class ?? Can this be done 
        #automatically (see self.__class__.__bases__ )

        if cName is None :
            self.cName = self.__class__.__name__
        else :
            self.cName = cName
            
        self.truncate = truncate
        
        self.RED     = "\033[1;31m"  
        self.BLUE    = "\033[1;34m"
        self.CYAN    = "\033[1;36m"
        self.GREEN   = "\033[0;32m"
        self.BROWN   = "\033[0;33m"
        self.PURPLE  = "\033[0;35m"
        self.YELLOW  = "\033[1;33m"
        self.RESET   = "\033[0;0m"
        self.BOLD    = "\033[;1m"
        self.REVERSE = "\033[;7m"
        
    """
        Black            \e[0;30m
        Blue             \e[0;34m
        Green            \e[0;32m
        Cyan             \e[0;36m
        Red              \e[0;31m
        Purple           \e[0;35m
        Brown            \e[0;33m
        Gray             \e[0;37m
        Dark Gray        \e[1;30m
        Light Blue       \e[1;34m
        Light Green      \e[1;32m
        Light Cyan       \e[1;36m
        Light Red        \e[1;31m
        Light Purple     \e[1;35m
        Yellow           \e[1;33m
        White            \e[1;37m
    """
    #--------------------------------------------------------------------------
    
    #--------------------------------------------------------------------------
    def setColor(self,*args):
        """
            Small wrapper for setting terminal color easily
        """
        marg = "".join(args)
        sys.stdout.write(marg)
        
    #--------------------------------------------------------------------------    
    def buildMessage(self,tipo="INFO", fields=(None) ):
            
        """
            Joins the fields in the input and builds the string for printout
            
            Parameters
            ----------
            tipo : str
                base name of what we wnat ot print (INFO, ERROR etc.)
            fields : tuple
                fields that will be converted to strings and appended to the 
                message for printing
                
            Returns
            -------
            msg : str
                string to be printed
                
            TODO :
                * truncate the message accoring to a number of chars
                * add color output (easy)
        """

        msg = " ----> " + tipo + " [" + self.cName + "]: "
        mlen = len(msg)
        msg = msg + " ".join(map(str,fields) )
        
        if type(self.truncate) is int :
            msg =  msg[0:mlen + self.truncate]

        return msg
    #--------------------------------------------------------------------------
    
    #--------------------------------------------------------------------------    
    def INFO(self, *args ):
        """
            Composes na prints INFO messages
        """
        self.setColor(self.GREEN)
        msg = self.buildMessage(tipo="INFO", fields=args)
        print msg
        self.setColor(self.RESET)
    #--------------------------------------------------------------------------
    
    #--------------------------------------------------------------------------   
    def WARNING(self, *args ):
        """
            Composes na prints WARNING messages
        """
        self.setColor(self.YELLOW)
        msg = self.buildMessage(tipo="WARNING", fields=args)    
        print msg
        self.setColor(self.RESET)
    #--------------------------------------------------------------------------
    
    #--------------------------------------------------------------------------
    def ERROR(self, *args ):
        """
            Composes na prints ERROR messages
        """
        self.setColor(self.RED)
        msg = self.buildMessage(tipo="ERROR", fields=args)  
        print msg
        self.setColor(self.RESET)
    #--------------------------------------------------------------------------