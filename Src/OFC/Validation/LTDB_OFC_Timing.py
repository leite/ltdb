"""
    Checks OFC timing derived from calibration using ionization waveforms
    This can start to also be a prototype to a more generic class to apply 
    the OFC coefs. for data reconstruction.
    
    before using, in USP cluster, do :

    setupATLAS ; lsetup "root 6.04.16-x86_64-slc6-gcc49-opt"
"""

import ROOT
import numpy as np
import pickle
import cPickle
import string
from collections import OrderedDict

from scipy import interpolate
import scipy

from   root_numpy import tree2array

from  LTDB_OFC import LTDB_OFC
from message import message as msg

class LTDB_OFC_Timing (msg) :
    """
        This class provides all the methods needed to proper assign and 
        reconstruct the waveform using calculated OFC's. It also implements
        plotting facilities for channel by channel validation
    """
    #===========================================================================
    def __init__(self,mappingFile=None,
                      dataDir=None, 
                      channelList = None) :
        
        """
            Prepares the processing by loading the mapping information and 
            creating a ROOT TChain from the data. 
            
            Parameters
            ----------
                mappingFile : str
                    Location of the Mapping file
                dataDir : str
                    Diretory containing the LTDB converted ROOT files
                channelList : list 
                    This is a list of tuples (iphi,scName) to be processed.
                    
            
            Todo :
                * raise exceptions if the files do not exist
                * change scCh to scName as it is a name 
                * verify that the number of channels and the number of events 
                  do not blows the memory. Devise a way to process also the
                  channels in chunks ??
        """
        
        cName = self.__class__.__name__
        msg.__init__(self, cName=cName)

        self.channelList = channelList
        self.iphi = 18
        
        #Get mapping information
        f = open(mappingFile)
        self.map = pickle.load(f)
        f.close()
        
        #get some  data
        self.tree = ROOT.TChain("LTDB")
        self.tree.Add(dataDir + "/*.root")
        self.tree.LoadTree(0)
    
        self.adcMin = 6
        self.adcMax = 1000
        
        self.aSamples = {}
        
        self.tagLTDB = {}
        
        for scCh in self.channelList :
            self.aSamples[scCh] = None
            self.tagLTDB[scCh] = OrderedDict()
    #===========================================================================
    
    #===========================================================================    
    def getOFC(self, ofcFileName) :
        """
            Call the method to handle the import of OFC from a root tree 
            (see LTDB_OFC class).  Note that entries with invalid tuple
            (layer, iphi, ieta) will be ignored. Basically, these are entries
            that have no correspondent key in mapping database.
        
            Parameters
            ----------
                ofcFileName : str
                    where to find the OFC ROOT Tree file
            
            Returns
            -------
                bool
                    True if successful, False otherwise.   
                                
            TODO: check that 
                1) which keys from mapping do not have OFC info
                2) All channels in channel list have an OFC defined
        """
    
        oo = LTDB_OFC()
        oo.readArray(ofcFileName)
        
        #move to my "internal format" for now
        self.ofc = {}
        
        #Which channels are on this OFC ?
        ss = set([tuple(i.astype(int)) for i in oo.ofc_r[:,0:3]])
        
        nmap = self.map["nameMap"]
        for sc in ss :
            try :
                scChannel = nmap[sc]
                self.ofc[scChannel] = {}
                #TODO: It has to be a more direct way to do this ....
                idx = np.where( (oo.ofc_r[:,0] == sc[0]) & 
                                (oo.ofc_r[:,1] == sc[1]) & 
                                (oo.ofc_r[:,2] == sc[2]) )

                for i in oo.ofc_r[idx[0]] :
                    #FIXME: needs a smarter way to get the num of OFC 
                    #from the  metadata
                    toff = i[3]
                    a = i[6:10]
                    b = i[10:14]
                    self.ofc[scChannel][i[3]] =  {"a":np.array(a), 
                                                  "b":np.array(b) }
            except KeyError :
                msg.WARNING(self, "No maping info for (layer, iphi, ieta) =", 
                                  sc, " - ignored" )
             
        return True
    #===========================================================================
        
    #---------------------------------------------------------------------------
    def getData(self,nev=10000):
        """
            Get data in chunks, or it may blow the memory 
            We also process data in each chunck in separate in order to keep 
            the memory profile low. Need to quantify it
            
            Parameters
            ----------
            nev : int
                number of events to be processed. If < 1 then process
                all availble events in TChain.
                
            Returns
            -------
            bool
                True if successful, False otherwise.
                
                
            Raises
            ------
        """
        
        nevTotal = self.tree.GetEntries()
        if nev <= 0 :
            nev = nevTotal
         
        msg.INFO (self, "Total number of events in tree is", nevTotal )
        msg.INFO (self, "Getting requested", nev, "events ..." )

        
        #Read in chunks of 20k events. This conserves RAM and is fast 
        #enough   
        step = 20000
        chunck = nev/step
        
        for ix in range (chunck) :
            start = ix*step
            stop = (ix+1)*step
            msg.INFO (self, "Now reading from", start, "to", stop) 
            data = tree2array(self.tree, start=start, stop=stop)
       
            #Look at all channels with some information
            self.getPulses(data)

            del (data)

    #---------------------------------------------------------------------------

    #---------------------------------------------------------------------------          
    def subPedestal(self,data=None, method="Direct") :
        """
            Implements pedestal subtraction using one of 
            three methods as described bellow (so far only one is 
            implemented)
            
            Parameters
            ----------
            data : numpy record array 
                arranged as (put ref here)
            method : str
                'Direct'  : 
                    take 1st sample of each waveform
                'Average' : 
                    take the average of 1st samples in each waveform 
                    for the data chunk passed
                'fromDB'  : 
                    takes from a pre-calculated pedestal loaded from a file. 
                    Falls back to 'Average' if no file information is loaded
                
            Returns
            -------
            samples : numpy array
                array of sample by sample pedestal subtracted
                
                
            Todo :
                * implement other methods besides 'Direct'
                
        """
        
        msg.INFO (self, "Subtracting pedestals")
        samples = data["samples"]
        
        if method == "Direct" :
            ped = samples[:,:,:,0]
            samples = np.transpose(samples) - np.transpose(ped)
            samples = np.transpose(samples)

        return samples
    #---------------------------------------------------------------------------
    
    #---------------------------------------------------------------------------    
    def getPulses(self,data="None"):
        """
            Implements the selection from the ABBA stream (sourceID, fiber, ch)
            to the offline channel name (iphi,scChannel) selecting only the
            channels within the self.adcMin and self.adcMax window. The events
            passing this selection have stored their (bcid, timeStamp) so we
            can match them with the main readout. At each iteration, a 
            dictionary where the key is the (iphi, scChannel) stores the
            waveforms stacked for further processing.
            
            We will NOT APPLY the OFC here : it is inconvenient for timing 
            offset analysis and we can always do elsewhere even for the 
            final OFCs. 
            
            Parameters
            ----------
                data : numpy record array
                    This is a record array read directly from a 'chunk' in 
                    from the  root tree. 
                
            Returns
            -------
                bool 
                True if successful, False otherwise.   
                
            Todo :
                * check the trigger 
                
            
        """
        
        samples = self.subPedestal(data, method="Direct")

        #FIXME: This needs to be better handled. 
        for scCh in self.channelList :
            mm = self.map["revMap"][scCh]
    
            #Extract the data stream position of a given scCh
            my_id = mm["id"]
            my_fiber = mm["fiber"]
            my_channel = mm["channel"]
        
            #Select only the events belonging to the respective section of LTDB 
            sid = data["sourceId"]
            sel_id = np.where(sid == my_id)
            idsamples = samples[sel_id]
            
            #Select only events within some ADC range 
            sel_samples = idsamples[:,my_fiber, my_channel, :]
            pk = np.max(sel_samples,axis=1)
            adc_cut = np.where( (pk>self.adcMin) & (pk<self.adcMax) )
            acut = adc_cut[0]
            self.xx = sel_samples[acut]
            
            #Include here the Event selection ta generation
            aTrig = data["l1"][sel_id]
            aBCID = data["bcid"][sel_id]
            aTime = data["tempo"][sel_id]
        
            self.aa = np.array([aTrig[acut],aBCID[acut],aTime[acut]]).transpose()
            for i in self.aa :
                self.tagLTDB[scCh][(i[1],i[2])] = i[0]
            
            lev = len(self.tagLTDB[scCh])
            msg.INFO(self,"Number of Events in ", scCh, " :", lev)
            
            #self.scx = np.array( [ self.xx,self.aa[:,1],self.aa[:,2] ] )
            #Stack the events for processing later
            self.scx = self.xx
            if self.aSamples[scCh] is None :
                self.aSamples[scCh] = self.scx
            else :
                self.aSamples[scCh] = \
                    np.vstack( (self.aSamples[scCh], self.scx ) )
                
        return True
    #===========================================================================
    
    #===========================================================================
    def findTiming(self, scChannel, method="ZeroCrossing"):
        """
            This routine will determine from a set of delays in the OFC 
            calculation the one that best describes the ionization data. 
            Currently 3 methods for this are available :
            - "ZeroCrossing" - where the E*t crosses zero is the optimum delay 
            - "Parabolic" fit of dispersion from several amplitudes, where the 
              delay of minimum dispersion is the optimum delay
            - "CFD" like - absolute determination of WFM timing   
            
            TODO: Find a suitable method to determine the start/stop 
                  of the waveform
             
        """
        #This needs to be better handled 
        wstart = 19
        wstop  = 23
        
        delayStart = -25.
        delayStop  =  25.
        
        gname = "iphi = " + str(self.iphi) + ", channel = " + scCh
        
        hname = "ZeroCrossing_" + scCh
        self.h_raiz = ROOT.TH1F(hname, hname, 250,-25,25) 
        self.h_raiz.SetXTitle("Zero Crossing [ns]")
        self.h_raiz.SetYTitle("Counts")
        
        hname = "Parabolic_" + scCh
        self.h_parb = ROOT.TH1F(hname, hname, 50,-25,24)
        self.h_parb.Sumw2()
        self.h_parb.SetXTitle("Delay [ns]")
        self.h_parb.SetYTitle("\sigma_{E * t} [Gev*ns]")
        self.h_parb.SetMarkerStyle(20)
        
        #Validate the input
        nv = self.aSamples[scCh].shape[0]
        if nv == 0 : 
            print "No suitable events in channel ", scCh
            print "No timming calculation is possible"
            return -1
        
        print " Found ", nv, "events to be used in timing finding"
        delay = np.arange(delayStart, delayStop)
        
        AA = []
        BB = []
        for ii in delay :
            aa = self.aSamples[scCh][:,19:23]
            AA.append( np.sum ( self.ofc[scCh][ii]["a"] * aa, 
                                axis=1) )
            BB.append( np.sum ( self.ofc[scCh][ii]["b"] * aa, 
                                axis=1) )
        self.AA = np.array(AA)
        self.BB = np.array(BB)
        
        #hardwired to get the amplitude for some channels
                
                
        if scChannel == "1A_P1" :
            didx = np.where(delay == 5)
        if scChannel == "1A_F1" :
            didx = np.where(delay == 9)
        if scChannel == "1A_M1" :
            didx = np.where(delay == 4)
        if scChannel == "1A_B1" :
            didx = np.where(delay == 11)  
                    
        if scChannel == "14A_P1" :
            didx = np.where(delay == 0)
        if scChannel == "14A_F1" :
            didx = np.where(delay == 4)
        if scChannel == "14A_M1" :
            didx = np.where(delay == -4)
        if scChannel == "14A_B1" :
            didx = np.where(delay == 0)
            
 
        


        didx = didx[0][0]
        print "*** ", didx
        #Assumes ordered dictionary, may not be true ...
        keys = self.tagLTDB[scChannel].keys() 
        for kk in range (len(keys)) :
            self.tagLTDB[scChannel][keys[kk]] = self.AA[didx,kk]
            print "======= ", kk, keys[kk], self.tagLTDB[scChannel][keys[kk]]
        
        if "ZeroCrossing" in method :
            #Interpolate each event as a function of delay
            print "In ZeroCrossing method ..."
            raizList = []
            tBB = self.BB.transpose()
            for bb in tBB :
                ib = np.where(bb>0)
                if len(ib[0]) > 7 :
                    inf = ib[0][-1] - 4
                    sup = ib[0][-1] + 5
                        
                    tx = delay[inf:sup]
                    vx = bb[inf:sup]
                    itx = interpolate.splrep(tx,vx,s=0,k=3)
                    raiz = interpolate.sproot(itx)
                    if len(raiz) > 1:
                        print "WARNING: More than one root found in ch ", scCh
                    try :
                        raizList.append(raiz[0])
                    except IndexError :
                        pass
                
            rm = np.mean(raizList)
            rs  = np.std(raizList)
            
            #Do it again in case of outliers
            #ir = np.where( (raizList > (rm-2*rs) ) & (raizList < (rm+2*rs) ) ) 
            
            #print raizList, ir
            #print raizList[ir]
            #rm = np.mean(raizList[ir])
            #rs = np.std(raizList[ir])
                           
            print "----> Optimum timing for ", scCh, ": ", rm, "+/-", rs
            
            map(self.h_raiz.Fill,raizList)
            #self.h_raiz.Draw()
        
        if "Parabolic" in method :
            print "In Parabolic method ..."
            #RMS for each delay for a group of events
            if self.BB.shape[1] > 10 :
                bstd = np.std(self.BB, axis=1)
                
                map(self.h_parb.Fill,delay,bstd)

                #self.gparb.Draw("ALP*")
            else :
                print "Need at least 10 events to calculate the optimum time using the parabolic method"
                return False
        
        if "CFD" in method :
            print "In CFD method ..."
            self.h_cfd = ROOT.TH1F("cfd","cfd",50,10,30)
            delay = 1
            frac = 0.3
            bias = 0
            t = np.arange(self.aSamples[scCh].shape[1])
            t2 = scipy.clip(t + delay,min(t),max(t))
            cfdList = []
            for v in self.aSamples[scCh] :
                #Need high amplitude signals
                if max(v) > 20 :
                    print t, v
                    it = interpolate.splrep(t,v,s=0,k=3)
                    v2 = frac*interpolate.splev(t2,it,der=0)
                    v3 = v2 - v + bias
    
                    itcfd = interpolate.splrep(t,v3,s=0)
                    raiz = interpolate.sproot(itcfd)
                    #Select only near the rising edge
                    ir = np.where( (raiz>16) & (raiz<23) )
                    print "---> CFD: ", raiz[ir]
                    try :
                        cfdList.append(raiz[ir][0])
                    except IndexError :
                        pass
                else :
                    print "Too low amplitude for CFD ... "
                    
            map(self.h_cfd.Fill, cfdList)
            
            
    #---------------------------------------------------------------------------
    def plotChannel(self,scChannel):
        

        
        nv = self.aSamples[scCh].shape[0]
        
        if nv == 0 : 
            print "No suitable events in channel ", scCh
            print "Nothing to plot ..."
        else :
            
            self.mgA = ROOT.TMultiGraph()
            self.mgB = ROOT.TMultiGraph()
            self.mgP = ROOT.TMultiGraph()
            
            self.h_raiz = ROOT.TH1F(scCh, scCh, 500,-50,50) 
            self.h_raiz.SetXTitle("Zero Crossing [ns]")
            self.h_raiz.SetYTitle("Counts")
            raizList = []
            for ev in range(nv) :
                aa = self.aSamples[scCh][ev][19:23]
                A = []
                B = []
                D = []      
                for i in range(-25,25) :
                    ii = 1. * i
                    #print scCh, ii
                    A.append(np.sum(x.ofc[scCh][ii]["a"] * aa ))
                    B.append(np.sum(x.ofc[scCh][ii]["b"] * aa ))
                    D.append(ii)
                    #print ii, A, B
                    
                DD = np.array(D)
                AA = np.array(A)
                BB = np.array(B)
            
                nsamples = self.aSamples[scCh].shape[1] 
                tt = np.arange(nsamples) * 1.0
                vv = self.aSamples[scCh][ev] * 1.0
                
                print tt, vv
                
                self.mgA.Add( ROOT.TGraph(len(AA), DD, AA) )
                self.mgB.Add( ROOT.TGraph(len(BB), DD, BB) )
                self.mgP.Add( ROOT.TGraph(nsamples, tt, vv) )
                
                
                #Interpolate the timing dependency so we can get and acurate
                #value of zero crossing
                
                ib = np.where(BB>0)
                
                self.BB = BB
                self.DD = DD
                
                if len(ib[0]) > 7 :
                    inf = ib[0][-1] - 4
                    sup = ib[0][-1] + 5
                    
                    
                    tx = DD[inf:sup]
                    vx = BB[inf:sup]
                    
                    
                    itx = interpolate.splrep(tx,vx,s=0,k=3)
                    raiz = interpolate.sproot(itx)
                    print scCh," Raiz ---> ", raiz
                    if len(raiz) > 0 :
                        self.h_raiz.Fill(raiz[0])
                        raizList.append(raiz[0])
                
            print scCh
            
            gname = "iphi = " + str(self.iphi) + ", channel = " + scCh
            self.c.cd(1)
            self.mgP.Draw("ALP")
            self.mgP.SetTitle(gname)
            self.mgP.GetXaxis().SetTitle("Sample #")
            self.mgP.GetYaxis().SetTitle("ADC [counts]")
            
            self.c.cd(2)
            self.mgA.SetTitle(gname)
            self.mgA.Draw("ALP")
            self.mgA.GetXaxis().SetTitle("Delay [ns]")
            self.mgA.GetYaxis().SetTitle("Energy [GeV]")
            
            self.c.cd(3)
            self.mgB.Draw("ALP")
            self.mgB.SetTitle(gname)
            self.mgB.GetXaxis().SetTitle("Delay [ns]")
            self.mgB.GetYaxis().SetTitle("Energy*t [Gev*ns]")
            
            self.c.cd(4)
            if len(raizList) > 4 :
                rmin = np.min(raizList) - 2
                rmax = np.max(raizList) + 2
                self.h_raiz.Draw()
                self.h_raiz.GetXaxis().SetRangeUser(rmin, rmax)
            self.h_raiz.Draw()
            
            ROOT.gPad.Update()
            self.c.Print(self.plotFileName)
    #--------------------------------------------------------------------------
    
    #--------------------------------------------------------------------------
    def matchCaloCell (self,scChannel):
        """
            Brute and violent for now ... 
            This is a realy dumb way of doing it
        """
        f = open ("run_311321CaloCell_evtList.pickle")
        self.tagCaloCell = cPickle.load(f)
        f.close()

        print "--------------> Number of Events in ", scChannel, " :", \
            len(self.tagLTDB[scChannel])
        sl = set(self.tagLTDB[scChannel].keys())
        sc = set(self.tagCaloCell.keys())
        
        self.si = sc.intersection(sl)
        
        print "Intersection gives ", len(self.si), " events"
        
        fn = "00311321_LTDB_selected_" + scChannel +".pickle"
        print "Saving into ", fn
        
        f = file(fn,"wb")
        pickle.dump(self.tagLTDB[scChannel], f)
        f.close()
        
        print "Done !"
#------------------------------------------------------------------------------
# END OF CLASS DEFINITION
#------------------------------------------------------------------------------
"""
if __name__ == "__main__":
    
    #ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetOptFit(11111)
    
    dataDir = "/data/data3/LAr/Data/00311321/LTDB/LAr_00311321/"
  
    mapName = "/home/leite/ABBA/LTDB/Analysis/ltdbMapping_v3.pickle"
    
    ofcDir_v0 = "/home/leite/ABBA/LTDB/Analysis/OFC_DB/v0/"
    ofcDir_v1 = "/home/leite/ABBA/LTDB/Analysis/OFC_DB/v1/"
    
    ofcDir = ofcDir_v0
    
    #FIXME A, 19->B (this should be better handled)
    iphi = 18 
    
    myChannelList = [ "1A_P1", "14A_P1", 
                      "1A_F1", "14A_F1", 
                      "1A_M1", "14A_M1", 
                      "1A_B1", "14A_B1" ]
    
    
    #myChannelList = ["14A_M1"]
    
    x = LTDB_OFC_Timing (mappingFile=mapName, 
                  dataDir=dataDir, 
                  ofcDir = ofcDir,
                  channelList = myChannelList)
    
    #Get the OFC information
    x.getOFC_v3("OFC_DB/ltdb_ofc.V_2017.Jan.25-14.07.26.root")
    
    
    x.getData(nev=350000)
    
    
    #mgA = {}
    #mgB = {}
    x.c.Divide(2,2)
    for scCh in myChannelList :
        x.plotChannel(scCh)
        
        #x.findTiming(scCh,method=["Parabolic", "ZeroCrossing", "CFD"])
        x.findTiming(scCh,method=["Nenhum"])
        x.matchCaloCell(scCh)
 
        x.c.cd(1)
        x.h_raiz.Draw()
        x.c.cd(2)
        x.h_parb.Draw("hist p l")
        ROOT.gPad.Update()
        
        #raw_input ("...")
    
        x.c.Print(x.plotFileName)
    x.c.Print(x.plotFileName + "]")
"""    
        