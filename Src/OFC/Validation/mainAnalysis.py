"""
    mainAnalysis.py : Steering routine to do the OFC analysis 
"""

import LTDB_OFC_Timing as LT


#The OFC directory and file to be used and 
ofcDatabaseDir = "/home/leite/LTDB/Database/OFC/"
ofcFileName = "ltdb_ofc.V_2017.Jan.27-20.51.58.root"
ofcFn = ofcDatabaseDir + ofcFileName

#The mapping file to use :
mapDir = "/home/leite/LTDB/Database/Mapping/"
mapFileName = "ltdbMapping_v3.pickle"
mapFn = mapDir + mapFileName

#Directory for the LTDB NTUPLE file (converted by abba_binary_reader_3.py)
dataDir = "/data/data3/LAr/Data/00311321/LTDB/LAr_00311321/"

myChannelList = [ (18,"1A_P1"), (18,"14A_P1"), 
                  (18,"1A_F1"), (18,"14A_F1"), 
                  (18,"1A_M1"), (18,"14A_M1"), 
                  (18,"1A_B1"), (18,"14A_B1") ]
    
ltdb = LT.LTDB_OFC_Timing (mappingFile=mapFn,  
                           dataDir=dataDir,
                           channelList = myChannelList)

ltdb.getOFC(ofcFileName=ofcFn)
ltdb.getData(60000)