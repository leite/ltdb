"""
    A main steering routine ...
    Before running, on lxplus :

    eosmount $HOME/eos
    setupATLAS
    lsetup "root  5.34.19-x86_64-slc5-gcc43-opt"
    
"""
import abba_binary_reader_3 as br3
from multiprocessing import Pool
import os

rootsys = os.environ["ROOTSYS"]
outputDir = "/tmp"
pickleMapFileName = "ltdbMapping.pickle"

#~/eos/atlas/atlastier0/tzero/prod/data16_13TeV/physics_L1Calo/00309759/data16_13TeV.00309759.physics_L1Calo.recon.AOD.x470

#dDir = "/afs/cern.ch/user/l/leite/eos/atlas/atlascerngroupdisk/det-larg/Demonstrator/Data/"
dDir = "/afs/cern.ch/user/l/leite/eos/atlas/atlascerngroupdisk/larg-upgrade/ABBA/ATLAS/rawdata/"

dFile = [
        #"data16_13TeV.00311322.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311323.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311324.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311325.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311326.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311327.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311328.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311329.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311330.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311331.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311334.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311336.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311337.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311340.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311341.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311342.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #
        #"data16_13TeV.00311288.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311289.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311290.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311291.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311292.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311293.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311294.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311295.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311296.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311297.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311298.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311299.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311300.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311301.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311303.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311308.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311310.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311311.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311312.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data", #This is a corrupted file
        #"data16_13TeV.00311323.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",        
        ###Run  311481
        #"data16_13TeV.00311482.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311486.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311491.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311495.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311498.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311503.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311505.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311507.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311510.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311516.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311518.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311521.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311523.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311528.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        ###Run 311402
        #"data16_13TeV.00311409.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311410.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311415.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311419.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311420.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311431.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311433.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311434.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311435.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311436.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311440.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311446.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311447.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        ###Run 311365
        #"data16_13TeV.00311366.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311369.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311371.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311372.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311373.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311374.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311375.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311376.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311377.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311378.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311379.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        ###Run 311473
        #"data16_13TeV.00311474.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data", 
        #"data16_13TeV.00311475.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311476.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00311477.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #Bill's calibration runs 
        #"data_test.00285014.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data",
        #"data_test.00278938.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data",
        #"data_test.00286191.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data",
        #"data_test.00286198.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data",
        #"data_test.00286202.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"
        #"data_test.00285003.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data", 
        #"data_test.00285005.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data",
        #"data_test.00285007.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data",
        #"data_test.00285010.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data",
        #
        #Check Philippe list
        #"data16_13TeV.00300657.calibration_LAR-demonstrator-readout.daq.RAW._lb0000._EB-EMF-05._0001.data",
        #"data16_13TeV.00300657.calibration_LAR-demonstrator-readout.daq.RAW._lb0000._EB-EMF-05._0002.data",
        #"data16_13TeV.00300657.calibration_LAR-demonstrator-readout.daq.RAW._lb0000._EB-EMF-05._0003.data",
        #"data16_13TeV.00309312.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        #"data16_13TeV.00309312.calibration_.daq.RAW._lb0000._EB-ABBA-01._0002.data",
        #"data16_13TeV.00309315.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data"
        #HIP    
        #"data16_hip8TeV.00313285.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data", 
	    #"data16_hip8TeV.00313285.calibration_.daq.RAW._lb0000._EB-ABBA-01._0002.data",
	    #"data16_hip8TeV.00313285.calibration_.daq.RAW._lb0000._EB-ABBA-01._0003.data",
	    #"data16_hip8TeV.00313285.calibration_.daq.RAW._lb0000._EB-ABBA-01._0004.data",
	    #"data16_hip8TeV.00313285.calibration_.daq.RAW._lb0000._EB-ABBA-01._0005.data",
	    #"data16_hip8TeV.00313285.calibration_.daq.RAW._lb0000._EB-ABBA-01._0006.data",
	    #"data16_hip8TeV.00313285.calibration_.daq.RAW._lb0000._EB-ABBA-01._0007.data",
	    #"data16_hip8TeV.00313285.calibration_.daq.RAW._lb0000._EB-ABBA-01._0008.data",
        #Calibration new
        "data16_calib.00313254.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        "data16_calib.00313554.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        "data16_calib.00313565.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        "data16_calib.00313567.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        "data16_calib.00313568.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data",
        "data16_calib.00313570.calibration_.daq.RAW._lb0000._EB-ABBA-01._0001.data"
	]


def procc(rf):
    #os.environ["ROOTSYS"] = rootsys
    outputDir = "/tmp"
    pickleMapFileName = "ltdbMapping.pickle"

    x = br3.ABBA_converter(rawDataFileName=dDir+rf,
                       outputDir=outputDir,
                       pickleMapFileName=pickleMapFileName,
                       DEBUG=0)

    x.eventDeclare()
    x.prepareData()
    x.getPayloadSize()
    x.processData()
    x.finalize()

nproc = len(dFile)
p = Pool(processes=nproc)
p.map(procc,dFile)
p.terminate()

