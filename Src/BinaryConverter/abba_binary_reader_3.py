'''
Created on Oct 1, 2014
Revised on Sept 20, 2016

@author: leite (leite@cern.ch)

Very simple ABBA reader and converter

        Besides ROOT, we require the following python packages
        numpy (pip install --user numpy)
        progressbar (pip install --user progressbar2 )
        root_numpy (pip install --user root_numpy)

        in lxplus, do
        lsetup "root  5.34.19-x86_64-slc5-gcc43-opt"
        pip install --user numpy
        pip install --user progressbar33
        pip install --user root_numpy

'''

#TODO: Color output
#TODO: exception check for file existence and map existence
#TODO: A method to print/show the header content for each fragment

import sys
import os
import time
import pickle
import collections

import progressbar as pgbr

import numpy as np

import ROOT

from root_numpy import array2tree

ROOT = ROOT

class ABBA_converter(object):
    '''
        Simple converter for ABBA format to unpack the ADC data and convert
        it to root Tree. Based on Jasmin unpacker and ATLAS event format specs.

        If available, a map file (in pickle format is read and saved together
        in the root file as a "metadata". Duriang analysis, this can be read
        and processed to get the offline geometry.

        Besides ROOT, we require the following python packages
        numpy (pip install --user numpy)
        progressbar (pip install --user progressbar33 )
        root_numpy (pip install --user root_numpy)

        in sls (eg lxplus) this does not work beacuse we need python 2.7
        and pip breaks when we set though setupATLAS.
        This still does not work ....

        lsetup root
        lsetup "sft --cmtconfig=x86_64-slc6-gcc47-opt external/lapack/3.4.0,
        external/pyanalysis/1.4_python2.7,
        external/blas/20110419"

    '''

    def __init__(self, rawDataFileName=None, outputDir=None,
                 pickleMapFileName=None, DEBUG=0):
        '''
            Open the file, call the definition of the header blocks and
            packet format
        '''
        self.DEBUG = DEBUG

        self.rawDataFileName=rawDataFileName
        self.outputDir=outputDir
        self.pickleMapFileName=pickleMapFileName

        print "\n"
        print 20*" ", "           *************                "
        print 20*" ", 40*"*"
        print 20*" ", "*** LTDB ABBA BINARY CONVERTER v3.0  ***"
        print 20*" ", 40*"*"
        print 20*" ", "           *************                "
        print "\n"

        if not self.validate () :
            print "    ... bailing out ..."
            print " "
            raise Exception()


        #File to save the Tree
        self.outFileName = self.rawDataFileName.replace(".data",".root")
        self.outFileName = self.outputDir + \
                            "/" +  os.path.basename(self.outFileName)
        self.outFile = ROOT.TFile(self.outFileName,"recreate")

        print "----> INFO: Will write output into ", outputDir, " :"
        print "----> INFO: ", self.outFileName
        #Get the mapping file
        print "----> INFO: Reading Mapping file: ", pickleMapFileName
        ff = open(pickleMapFileName)
        self.ltdbMap = pickle.load(ff)

        self.start = time.time()
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
    def validate(self):
        #First, check if all parameters are passed :
        cond1 = self.rawDataFileName == None
        cond2 = self.outputDir == None
        cond3 = self.pickleMapFileName == None
        if cond1 or cond2 or cond3 :
            print "-----> ERROR: Please provide the raw data file name,", \
                  " output directory and pickle mapping file"
            return False

        #Check for the input file
        try:
            fd = os.open(self.rawDataFileName,  os.O_EXCL | os.O_RDONLY)
            os.close(fd)
            if self.rawDataFileName.find("eos") :
                print "-----> WARNING: ",
                print "Detected eos file, this can be slow sometimes"
        except OSError, e:
            print "-----> ERROR: ", e
            return False

        #Check for the pickle mapping file
        try:
            fd = os.open(self.pickleMapFileName,  os.O_EXCL | os.O_RDONLY)
            os.close(fd)
        except OSError, e:
            print "-----> ERROR: ", e
            return False

        #Verify we can write the data to the output dir
        if not os.access(self.outputDir, os.W_OK) :
            print "-----> ERROR: Cannot write to  ", self.outputDir
            return False

        return True
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
    def eventDeclare(self):

        """
            Define the header names and structures. The dictionary value
            holds the position of the field named by the key in the fragment.
        """
        #TODO Use OrderedDict to keep this order

        self.event = collections.OrderedDict()
        self.event["marker"] = 0
        self.event["fragmentSize"] = 1
        self.event["headerSize"] = 2
        self.event["formatVersion"] = 3
        self.event["sourceId"] = 4
        self.event["numStatusElements"] = 5
        self.event["status"] = 6
        self.event["numSpecificHeaders"] = 7
        self.event["time"] = 8
        self.event["timeNs"] = 9
        self.event["eventID_LSB"] = 10
        self.event["eventID_MSB"] = 11
        self.event["runType"] = 12
        self.event["runNumber"] = 13
        self.event["lumiBlock"] = 14
        self.event["extEventID"] = 15
        self.event["bcid"] = 16
        self.event["L1TriggerType"] = 17
        self.event["CompressionType"] = 18
        self.event["UncrompressedPayloadSize"] = 19
        self.event["L1"] = 20
        self.event["L2"] = 21
        self.event["EF"] = 22
        self.event["stream"] = 23

        self.rob = collections.OrderedDict()
        self.rob["marker"] = 0
        self.rob["fragmentSize"] = 1
        self.rob["headerSize"] = 2
        self.rob["formatVersion"] = 3
        self.rob["L1TriggerType"] = 4
        self.rob["numStatusElements"] = 5
        self.rob["t1"] = 6
        self.rob["t2"] = 7
        self.rob["t3"] = 8

        self.rod = collections.OrderedDict()
        self.rod["marker"] = 0
        self.rod["headerSize"] = 1
        self.rod["formatVersion"] = 2
        self.rod["sourceId"] = 3
        self.rod["runNumber"] = 4
        self.rod["L1AId"] = 5
        self.rod["bcid"] = 6
        self.rod["L1ATriggerType"] = 7
        self.rod["detectorEventType"] = 8
        self.rod["payload"] = 9
        self.rod["numStatusElements"] = -1    #Need to know payload size
        self.rod["numDataElements"] = -2      #Need to know payload size
        self.rod["statusBlockPosition"] = -3  #Need to know payload size

        #These are the markers for each type of fragment
        self.marker = {"Event": 0xaa1234aa,
                       "ROB":   0xdd1234dd,
                       "ROD":   0xee1234ee }

        #nSAmples * nChannels * nFibers = 1600
        #each samples is 12 bits -> 1600 * 12 = 19200
        #each word is 32 bits -> 19200 / 32 = 600 -> 600 words
        self.nChannels = 8
        self.nFibers   = 20
        self.nADCBits  = 12

        #This is how the channells are organized in a sequence
        #inside the payload
        self.fiberSeq = [0,0,1,1,0,0,1,1,0,0,1,1,0,0,1,1]
        self.adcSeq =   [1,0,1,0,3,2,3,2,5,4,5,4,7,6,7,6]
        self.faseq = zip(self.fiberSeq,self.adcSeq)

        #Now, create a simple array to store the fiber and channel in the TTree
        self.channelArray = np.array([ np.arange(self.nChannels)]*self.nFibers)
        fList = [ [i]*self.nChannels for i in range (self.nFibers) ]
        self.fiberArray   = np.array(fList)


        self.tree = None
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
    def prepareData(self):
        """
            Read the data files, extract the markers an print out some
            information about this file
        """
        #TODO: Check that this is really an ATLAS raw data file

        print "----> INFO: Reading Data File: ", self.rawDataFileName
        self.data = np.fromfile(self.rawDataFileName, dtype=np.uint32, count=-1)

        self.eventDeclare()

        #Extract the indexes of header, rob and rod
        print "----> INFO: Extracting markers ... "
        self.indexEvent = np.where(self.data == self.marker["Event"])[0]
        self.indexROB   = np.where(self.data == self.marker["ROB"])[0]
        self.indexROD   = np.where(self.data == self.marker["ROD"])[0]

        #sanit check
        print "----> INFO: Number of event headers :", len(self.indexEvent)
        print "----> INFO: Number of ROB fragments :", len(self.indexROB)
        print "----> INFO: Number of ROD fragments :", len(self.indexROD)

        idx = self.indexROD + self.rod["sourceId"]


        idx0 = self.indexEvent[0]
        runNumber = self.data[ idx0 + self.event["runNumber"] ]
        print "----> INFO: Run Number : ", runNumber

        runType = hex(self.data[ idx0 + self.event["runType"] ])[:-1]
        print "----> INFO: Run Type : ", runType

        sourceIds = set(self.data[idx])
        print "----> INFO: Source Ids in this run: ", map(hex,sourceIds)

        l1Trigger = self.data[self.indexEvent + self.event["L1TriggerType"]]
        l1 = np.unique(l1Trigger,return_counts=True)
        print "----> INFO: L1 triggers in this run: ", map(hex,l1[0])
        for i in range(len(l1[0])) :
            print "---------> INFO: Number of ", hex(l1[0][i])[:-1], \
                  " triggers : ",l1[1][i]
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
    def getPayloadSize(self):
        """
            Extract the payload and number of samples based on the indexes
        """
        fragmentSize  = self.data[ self.indexROB + self.rob["fragmentSize"] ]
        robHeaderSize = self.data[ self.indexROB + self.rob["headerSize"] ]
        rodHeaderSize = self.data[ self.indexROD + self.rod["headerSize"] ]

        #Now get the number of samples :
        #this comes from the optical header, which is the 1st element after
        #the end of the ROD header
        self.sampleSize = self.data[ self.indexROD + self.rod["payload"] ]
        ssu = np.unique(self.sampleSize)
        print "----> INFO: Number of samples present: ", ssu
        #TODO: what to do if more than one sample size is present ?
        #(may pass the sample size array to unpack?)
        self.nSamples = ssu[0]

        self.payloadSize =  fragmentSize - robHeaderSize - rodHeaderSize - 3
        print "----> INFO: Payload sizes: ", np.unique(self.payloadSize)


        expectedPayloadSize = (ssu*self.nChannels*self.nFibers*self.nADCBits )
        expectedPayloadSize =  expectedPayloadSize/32 + 1 #in bits
        print "----> INFO: Expect payload sizes: ", expectedPayloadSize
        #TODO: Warning, ERROR if the expected and the got is different

        #I will need this information to fill the TTree, better get it now
        self.eventInfo = {}
        self.eventInfo["evtId_LSB"]  = self.data[ self.indexEvent +
                                                 self.event["eventID_LSB"] ]
        self.eventInfo["evtId_MSB"]  = self.data[ self.indexEvent +
                                                 self.event["eventID_MSB"] ]
        self.eventInfo["runNumber"]  = self.data[ self.indexEvent +
                                                 self.event["runNumber"] ]
        self.eventInfo["lumiblock"]  = self.data[ self.indexEvent +
                                                 self.event["lumiBlock"] ]
        self.eventInfo["bcid"]       = self.data[ self.indexEvent +
                                                 self.event["bcid"] ]
        self.eventInfo["time"]       = self.data[ self.indexEvent +
                                                 self.event["time"] ]
        self.eventInfo["timeNs"]     = self.data[ self.indexEvent +
                                                 self.event["timeNs"] ]
        self.eventInfo["l1TrigType"] = self.data[ self.indexEvent +
                                                 self.event["L1TriggerType"] ]
        self.eventInfo["l1Counter"]  = self.data[ self.indexROD   +
                                                 self.rod["L1AId"] ]
        self.eventInfo["sourceId"]   = self.data[ self.indexROD   +
                                                 self.rod["sourceId"] ]
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
    def processData (self):
        """
            Scan every fragment and call the unpack method for each fragment,
            then fill in the tree
        """

        print "----> INFO: Processing ", len(self.indexROD), " fragments "
        nFragments = len(self.indexROD)
        widgets = ['Processing: ',
                   pgbr.Percentage(), ' ',
                   pgbr.Bar(marker=pgbr.RotatingMarker()),' ',
                   pgbr.ETA() ]
        pbar = pgbr.ProgressBar(widgets=widgets, maxval=nFragments)
        pbar.start()
        #TODO: numpythonize this ...
        for i in range(len(self.indexROD)) :
            if (self.DEBUG > 0) :
                self.printHeaders(i)
            pbar.update(i)
            start = self.indexROD[i] + self.rod["payload"]
            stop  = start + self.payloadSize[i]
            z = self.data[start:stop]

            #The relevant information for the Ntuple
            adcData = self.unpack(z)

            self.fillTree(samples=adcData,
                          evt=i,
                          evtId_LSB  = self.eventInfo["evtId_LSB"][i],
                          evtId_MSB  = self.eventInfo["evtId_MSB"][i],
                          runNumber  = self.eventInfo["runNumber"][i],
                          lumiblock  = self.eventInfo["lumiblock"][i],
                          bcid       = self.eventInfo["bcid"][i],
                          tempo      = self.eventInfo["time"][i],
                          tempoNs    = self.eventInfo["timeNs"][i],
                          l1TrigType = self.eventInfo["l1TrigType"][i],
                          l1Counter  = self.eventInfo["l1Counter"][i],
                          sourceId   = self.eventInfo["sourceId"][i] )
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
    def unpack(self,payload):
        """
            Unpack the ABBA payload to a numpy
            array of dim [fibers,channels, samples]
        """

        #The first one is the optical header
        z = payload[1:]
        z.shape = (len(z)/6,6)
        z = np.array(z,dtype="O")
        for k in range(5) :
            z[:,k] = z[:,k]<<(160 - k*32)
        ww = z.sum(1)

        adcData = np.zeros(self.nChannels * self.nFibers * self.nSamples,
                           dtype=int)
        adcData.shape = (self.nFibers, self.nChannels, self.nSamples)

        for fiber in range (0,self.nFibers,2) :
            for sample in range(self.nSamples) :
                zx = sample + (fiber/2)*self.nSamples
                wx = ww[zx]
                k = 0
                for fa in self.faseq :
                    fb = fa[0]+fiber
                    ch = fa[1]
                    wval = wx>>(180-(12*k)) & 0xfff
                    adcData[fb,ch,sample] = wval
                    k += 1

        return adcData
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
    def fillTree(self, samples, evt, evtId_LSB, evtId_MSB,
                 runNumber, lumiblock, bcid, tempo, tempoNs,
                 l1TrigType, l1Counter, sourceId ):
        """
            Fill the tree with the relevant information using root_numpy
        """

        aTuple = (evt, evtId_LSB, evtId_MSB,
                  runNumber, lumiblock, bcid,
                  tempo, tempoNs,
                  l1TrigType, l1Counter, sourceId,
                  self.fiberArray, self.channelArray,
                  samples)

        fbShape = self.fiberArray.shape
        chShape = self.channelArray.shape
        spShape = samples.shape

        adc = np.array([aTuple], dtype=[ ("event",int),
                                         ("evtId_LSB",int),
                                         ("evtId_MSB",int),
                                         ("runNumber",int),
                                         ("lumiblock",int),
                                         ("bcid",int),
                                         ("tempo",int),
                                         ("tempoNs",int),
                                         ("l1",int),
                                         ("l1Counter",int),
                                         ("sourceId", int),
                                         ("fiber",int,fbShape),
                                         ("channel",int,chShape),
                                         ("samples",int,spShape)] )

        if self.tree is None :
            self.tree = array2tree(adc, name="LTDB")
        else :
            array2tree(adc, name="LTDB", tree=self.tree)
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
    def finalize(self):
        """
            Last things to be done
        """
        print "----> INFO: Saving tree to file: ", self.outFileName
        self.tree.Write()
        self.outFile.Close()

        fragments = len(self.indexEvent)
        dTime = time.time() - self.start
        fragSeg = int(fragments/dTime)

        print "----> INFO: ", fragments, " fragments processed in ", \
              int(dTime) ," seconds (", fragSeg, " fragments/s).."
        print "----> INFO: Goodbye..."
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
    def printHeaders(self,idx):
        """
            Used to show the header content of the ith fragment
        """

        toPrint = {"EVENT HEADER":(self.event, self.indexEvent),
                   "ROB HEADER":  (self.rob, self.indexROB),
                   "ROD HEADER":  (self.rod, self.indexROD),}
        print " "
        for key,val in zip(toPrint.keys(), toPrint.values()) :
            print "---------------------------------------------------------- "
            for tipo,offset in zip(val[0].keys(), val[0].values()) :
                pt = val[1][idx] + offset
                print "-- ", key, ": ", tipo, hex(self.data[pt])
        print "-------------------------------------------------------------- "
        print " "
#------------------------------------------------------------------------------
# END OF CLASS DEFINITION
#------------------------------------------------------------------------------

if __name__ == "__main__":

    nArgs = len(sys.argv)
    if nArgs != 3:
        sys.exit("usage: abba_binary_reader.py <input file> <output_dir>\n")

    #Files are located in
    #~/eos/atlas/atlascerngroupdisk/det-larg/Demonstrator/Data/

    rawDataFileName = sys.argv[1]
    outputDir = sys.argv[2]

    pickleMapFileName = "ltdbMapping.pickle"

    x = ABBA_converter(rawDataFileName=rawDataFileName,
                       outputDir=outputDir,
                       pickleMapFileName=pickleMapFileName,
                       DEBUG=0)

    x.eventDeclare()
    x.prepareData()
    x.getPayloadSize()
    x.processData()
    x.finalize()


